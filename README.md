# Laravel Vue Real Time Chat For AIST Global

## Project installation

### dependencies

* PHP 7.3 | ^8.0
* MySQL ^5.7
* Composer 2.*
* Pusher-php-serve ^6.1
* Node v12.20.0
* NPM 7.8.0

### installation

* Install composer: `composer install`
* Make `.env` file: `cp .env.example .env`
* Generate your `APP_KEY`: `php artisan key:generate`
* Generate `JWT SECRET`: `php artisan jwt:secret`
* Make `DB` and change `DATABASE CONFIGS` in `.env` file
* Set your pusher configs on `.env` file
* Set your email configs on `.env` file
* Run migrations and seeders: `php artisan migrate --seed`
* Install node_modules `npm install`
* Build front-end `npm run prod` ( for development run `npm run watch` )
* Run server: `php artisan serve`
* open with browser [127.0.0.1:8000](http://127.0.0.1:8000)
* for queue run `php artisan queue:listen`
* On the server make crone job for run `php artisan messages:remove-olds` every day at `09:15 AM`

