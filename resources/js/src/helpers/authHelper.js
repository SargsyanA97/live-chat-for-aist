import store from '../store'
import {SET_AUTH} from "../store/mutations";
import apiFactory from '../api';
import router from "../roues";
const authApi = apiFactory.get('auth');

export const isAuth = (to) => {
    const api_token = localStorage.getItem('api_token');

    if (!api_token || !store.getters.getAuth) {

        const redirectObj = {
            name: 'login',
        }

        if (to.fullPath !== '/') {
            redirectObj.query = to.fullPath
        }

        router.push(redirectObj);
    }
}


export const isGuest = () => {
    const api_token = localStorage.getItem('api_token');

    if (api_token && store.getters.getAuth) {
        router.push({
            name: 'home',
        });
    }
}

export const setAuthIfExists = async () => {
    const api_token = localStorage.getItem('api_token');

    if (!api_token) {
        store.commit(SET_AUTH, null);
    }

    try {
        const headers = {
            Authorization: localStorage.getItem('api_token')
        }

        const res = await authApi.getAuth({
            headers
        });

        store.commit(SET_AUTH, res.data.user);

    } catch (error) {
        store.commit(SET_AUTH, null);
    }
}

