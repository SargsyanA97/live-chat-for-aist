import localization from "./modules/localization";
import auth from "./modules/auth";
import user from "./modules/user";
import chatMessages from "./modules/chatMessages";

const modules = {
    localization,
    auth,
    user,
    chatMessages
}

export default {
    get: name => modules[name]
}
