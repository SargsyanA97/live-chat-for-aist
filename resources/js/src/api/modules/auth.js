import client from "../client";
const resource = 'auth'

export default {
    register: async payload => await client.post(`${resource}/register`, payload),
    login: async payload => await client.post(`${resource}/login`, payload),
    getAuth: async headers => await client.get(`${resource}/user`, headers),
    logout: async headers => await client.post(`${resource}/logout`, headers),
}
