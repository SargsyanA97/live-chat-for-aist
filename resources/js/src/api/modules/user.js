import client from "../client";
const resource = 'users'

export default {
    getAllUsers: async headers => await client.get(`${resource}`, headers)
}
