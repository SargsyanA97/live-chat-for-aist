import client from "../client";
const resource = 'messages'

export default {
    getMessages: async (payload, headers) => await client.get(`${resource}/${payload}`, headers),
    sendMessage: async (payload, headers) => await client.post(`${resource}`, payload, headers)
}
