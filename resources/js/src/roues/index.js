import VueRouter from 'vue-router'
import main from './main';
import {isAuth, isGuest, setAuthIfExists} from '../helpers/authHelper'

// Routes
const routes = [...main];

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
});

router.beforeEach((to, from, next) => {
    (async () => {
        await setAuthIfExists();
        to.matched.some(record => record.meta.requiredAuth) ? isAuth(to) : next();
        to.matched.some(record => record.meta.requiredGuest) ? isGuest() : next();
    })();

});

export default router;
