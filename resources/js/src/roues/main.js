const Home = () => import('../pages/main/Home');
const Register = () => import('../pages/main/Register');
const Login = () => import('../pages/main/Login');
import MainApp from '../layouts/MainApp';

export default [
    {
        path: '/',
        name: 'home',
        component: MainApp,
        children: [
            {
                path: '/',
                name: 'home',
                component: Home,
            },
            {
                path: '/register',
                name: 'register',
                component: Register,
                meta: {
                    requiredGuest: true,
                }
            },
            {
                path: '/login',
                name: 'login',
                component: Login,
                meta: {
                    requiredGuest: true,
                }
            },
        ]
    },
];
