import {SET_USERS, SET_CHAT_OVERLAY} from "../mutations";
import apiFactory from "../../api";
const userApi = apiFactory.get('user');


export default {
    state: {
        users: null,
        chatOverlay: true,
    },
    getters: {
        getAllUsers: state => state.users,
        getChatOverlay: state => state.chatOverlay,
    },
    mutations: {
        [SET_USERS]: (state, payload) => state.users = payload,
        [SET_CHAT_OVERLAY]: (state, payload) => state.chatOverlay = payload,
    },
    actions: {
        async setUsers({commit}) {
            try {
                const headers = {
                    Authorization: localStorage.getItem('api_token')
                }

                const res = await userApi.getAllUsers({
                    headers
                });

                commit(SET_USERS, res.data.users);
                commit(SET_CHAT_OVERLAY, false);
            } catch (error) {
                console.log(error);
            }
        },
        async searchUsers({commit}, params) {
            try {
                const headers = {
                    Authorization: localStorage.getItem('api_token')
                }

                const res = await userApi.getAllUsers({
                    headers,
                    params
                });

                commit(SET_USERS, res.data.users);
            } catch (error) {
                console.log(error);
            }
        }
    }
}
