import {SET_RESPONSE_ERRORS} from "../mutations";


export default {
    state: {
        responseErrors: null
    },
    getters: {
        getResponseErrors: state => state.responseErrors,
    },
    mutations: {
        [SET_RESPONSE_ERRORS]: (state, payload) => state.responseErrors = payload,
    },
    actions: {
        setResponseErrors({commit}, payload) {
            commit(SET_RESPONSE_ERRORS, payload)
        }
    }
}
