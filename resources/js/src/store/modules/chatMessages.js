import {
    SET_CHAT_MESSAGES,
    SET_RESPONSE_ERRORS,
    SET_SELECTED_PERSON_ID,
    SET_CHAT_OVERLAY
} from "../mutations";
import apiFactory from '../../api';
const chatMessagesApi = apiFactory.get('chatMessages');


export default {
    state: {
        chatMessages: null,
        selectedPersonId: null,
    },
    getters: {
        getSelectedPersonId: state => state.selectedPersonId,
        getChatMessages: state => {
            return state.chatMessages
        },
    },
    mutations: {
        [SET_CHAT_MESSAGES]: (state, payload) => state.chatMessages = payload,
        [SET_SELECTED_PERSON_ID]: (state, payload) => state.selectedPersonId = payload,
    },
    actions: {
        setSelectedPersonId({commit, getters}, payload) {
            const users = getters.getAllUsers;

            users.map(user => {
                if (user.id === payload) {
                    user.messages_count = 0;
                }
            });

            commit(SET_SELECTED_PERSON_ID, payload)
        },
        async setChatMessages({commit, getters}, payload) {
            try {
                commit(SET_CHAT_OVERLAY, true)
                const headers = {
                    Authorization: localStorage.getItem('api_token')
                }

                const res = await chatMessagesApi.getMessages(payload, {
                    headers
                })

                commit(SET_CHAT_MESSAGES, res.data.messages);
                commit(SET_CHAT_OVERLAY, false);
            } catch (error) {
                commit(SET_RESPONSE_ERRORS, {error: [getters.getTranslationText('Something_went_wrong')]});
                commit(SET_CHAT_OVERLAY, false)
                console.log(error);
            }
        },

        async sendMessage({commit, getters}, payload) {
            const headers = {
                Authorization: localStorage.getItem('api_token')
            }

            payload.to_user_id = getters.getSelectedPersonId;
            commit(SET_CHAT_OVERLAY, true)
            try {
                await chatMessagesApi.sendMessage(payload, {
                    headers
                });

                commit(SET_CHAT_OVERLAY, false)
            } catch (error) {
                commit(SET_RESPONSE_ERRORS, {error: [getters.getTranslationText('Something_went_wrong')]});
                commit(SET_CHAT_OVERLAY, false)
                console.log(error);
            }
        },

        addCatMessage({commit, getters}, payload) {
            commit(SET_CHAT_OVERLAY, true);

            if (
                getters.getSelectedPersonId === payload.from_user_id ||
                getters.getSelectedPersonId === payload.to_user_id
            ) {
                const allMessages = getters.getChatMessages;
                allMessages.push(payload);
                commit(SET_CHAT_MESSAGES, allMessages);
            } else {
                const users = getters.getAllUsers;
                users.map(user => {
                    if (payload.from_user_id === user.id) {
                        user.messages_count++;
                    }
                });
            }

            commit(SET_CHAT_OVERLAY, false)
        }
    }
}
