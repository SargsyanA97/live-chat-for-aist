import apiFactory from '../../api';
import {SET_RESPONSE_ERRORS, SET_AUTH, SET_CHAT_MESSAGES, SET_USERS} from "../mutations";
import router from "../../roues";
import store from "../index";
const authApi = apiFactory.get('auth');


export default {
    state: {
        auth: null
    },

    getters: {
        getAuth: state => state.auth,
    },

    mutations: {
        [SET_AUTH]: (state, payload) => state.auth = payload,
    },

    actions: {
        async authRegister({commit, getters}, payload) {
            try{
                const res = await authApi.register(payload);
                commit(SET_AUTH, res.data.user)
                localStorage.setItem('api_token', 'Barer ' + res.headers.authorization);
                await router.push({name: 'home'})
            } catch (error) {
                if (error?.response?.data?.errors) {
                    commit(SET_RESPONSE_ERRORS, error.response.data.errors);
                } else {
                    commit(SET_RESPONSE_ERRORS, {error: [getters.getTranslationText('Something_went_wrong')]});
                    console.log(error);
                }
            }
        },
        async authLogin({commit, getters}, payload) {
            try{
                const res = await authApi.login(payload);
                commit(SET_AUTH, res.data.user)
                localStorage.setItem('api_token', 'Barer ' + res.headers.authorization);
                await router.push({name: 'home'})
            } catch (error) {
                if (error?.response?.data?.errors) {
                    commit(SET_RESPONSE_ERRORS, error.response.data.errors);
                } else {
                    commit(SET_RESPONSE_ERRORS, {error: [getters.getTranslationText('Something_went_wrong')]});
                    console.log(error);
                }
            }
        },
        async authLogout({commit}) {
            try{
                const headers = {
                    Authorization: localStorage.getItem('api_token')
                }

                await authApi.logout({
                    headers
                });

                store.commit(SET_AUTH, null);
                store.commit(SET_CHAT_MESSAGES, null);
                store.commit(SET_USERS, null);
                localStorage.removeItem('api_token');
                await router.push({name: 'home'})
            } catch (error) {
                if (error?.response?.data?.errors) {
                    commit(SET_RESPONSE_ERRORS, error.response.data.errors);
                } else {
                    console.log(error);
                }
            }
        }
    },
}
