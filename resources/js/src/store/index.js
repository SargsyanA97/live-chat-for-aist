import Vue from 'vue';
import Vuex from 'vuex';
import localization from "./modules/localization";
import errors from "./modules/errors";
import auth from './modules/auth'
import user from './modules/user'
import chatMessages from './modules/chatMessages'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        localization,
        auth,
        user,
        chatMessages,
        errors
    },
});
