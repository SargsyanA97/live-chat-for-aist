<?php

namespace App\Console\Commands;

use App\Modules\Messages\Models\Message;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class RemoveOldMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:remove-olds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $carbon  = Carbon::now();
        $date =  $carbon->subDays(1)->format('Y-m-d H:i:s');

        Message::where('created_at', '<=', $date)->delete();

        $this->info("successfully deleted");
    }
}
