<?php

namespace App\Modules\Auth\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\ApiBaseController;
use App\Modules\Auth\Requests\AuthLoginRequest;
use App\Modules\Auth\Requests\AuthRegisterRequest;
use App\Modules\Auth\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends ApiBaseController
{
    /**
     * @var AuthService
     */
    private AuthService $service;

    /**
     * UserController constructor.
     * @param AuthService $service
     */
    public function __construct(AuthService $service)
    {
        $this->service = $service;
    }

    public function register(AuthRegisterRequest $request): JsonResponse
    {
        $this->service->register($request);
        $credentials = $request->only('email', 'password');

        if ($token = $this->service->login($credentials)) {
            return $this->setResponseHeaders(['Authorization' => $token])
                ->responseWithData([
                    'user' => $request->user()
                ]);
        }

        return $this->responseWithError([
            'server' => [trans('static.Something_went_wrong')]
        ]);
    }

    /**
     * @param AuthLoginRequest $request
     * @return JsonResponse
     */
    public function login(AuthLoginRequest $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->service->login($credentials)) {
            return $this->setResponseHeaders(['Authorization' => $token])
                ->responseWithData([
                    'user' => $request->user()
                ]);
        }

        return $this->responseWithError([
                'email' => [trans('static.Email_or_password_is_incorrect')]
            ],
            ResponseHelper::RESPONSE_CODE_UNAUTHORIZED
        );
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $this->service->logout();

        return $this->responseWithMessage(trans('static.User_log_outed'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function user(Request $request): JsonResponse
    {
        $user = $request->user();

        return $this->responseWithData(compact('user'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function refresh(Request $request): JsonResponse
    {
        if ($token = $this->service->refresh()) {
            return $this->setResponseHeaders(['Authorization' => $token])
                ->responseWithData([
                    'user' => $request->user()
                ]);
        }

        return $this->responseWithMessage(
            trans('static.Unauthorized'),
            ResponseHelper::RESPONSE_CODE_UNAUTHORIZED
        );
    }
}
