<?php

namespace App\Modules\User\Controllers;

use App\Http\Controllers\ApiBaseController;
use App\Modules\User\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class UserController extends ApiBaseController
{
    /**
     * @var UserService
     */
    private UserService $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllUsers(Request $request): JsonResponse
    {
        $users = $this->service->getAllUsers($request->get('q'));

        return $this->responseWithData(compact('users'));
    }
}
