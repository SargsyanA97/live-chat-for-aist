<?php

namespace App\Modules\User\Models;

use App\Modules\Messages\Models\Message;
use \Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string password
 * @method select(string[] $array)
 */

class User extends \App\Models\User
{
    /**
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'from_user_id')
            ->where('viewed', 0)
            ->where('to_user_id', request()->user()->id);
    }
}
