<?php

namespace App\Modules\User\Services;

use App\Modules\User\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;

class UserService
{
    /**
     * @var UserRepository
     */
    private UserRepository $repository;

    /**
     * UserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string|null $query
     * @return array|Collection
     */
    public function getAllUsers(null|string $query): array|Collection
    {
        return $this->repository->getAllUsers($query);
    }
}
