<?php
use Illuminate\Support\Facades\Route;
use App\Modules\User\Controllers\UserController;

Route::group(['middleware' => 'auth:api', 'prefix' => 'users'], function () {
    Route::get('/', [UserController::class, 'getAllUsers']);
});
