<?php

namespace App\Modules\User\Repositories;

use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class UserRepository
{
    /**
     * @var User
     */
    protected User $model;

    /**
     * AuthRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param string|null $query
     * @return array|Collection
     */
    public function getAllUsers(null|string $query): array|Collection
    {
        $users = $this->model->select(['id', 'first_name', 'last_name'])->where('id', '!=', request()->user()->id);

        if (!empty($query)) {
            $users->where(DB::raw('concat(first_name," ",last_name)'), 'like', '%'.$query.'%');
        }

        return $users->withCount('messages')->get();
    }
}
