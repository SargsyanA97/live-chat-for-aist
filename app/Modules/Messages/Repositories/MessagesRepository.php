<?php

namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\Message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class MessagesRepository
{
    /**
     * @var Message
     */
    protected Message $model;

    /**
     * AuthRepository constructor.
     * @param Message $model
     */
    public function __construct(Message $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $id
     */
    public function setAllMessagesToViewed(string $id): void
    {
        $this->model->where([
            'to_user_id' => request()->user()->id,
            'from_user_id' => $id
        ])->update(['viewed' => 1]);
    }

    /**
     * @param int $id
     * @return Model|Collection|Builder|array|null
     */
    public function getOneMessage(int $id): Model|Collection|Builder|array|null
    {
        return $this->model->with(['user', 'toUser'])->find($id);
    }

    /**
     * @param string $id
     * @return Collection|array
     */
    public function getMessages(string $id): Collection|array
    {
        $this->setAllMessagesToViewed($id);

        return $this->model
            ->with('user')
            ->where([
                'to_user_id' => request()->user()->id,
                'from_user_id' => $id
            ])
            ->orWhere(function ($query) use ($id) {
                $query->where([
                    'from_user_id' => request()->user()->id,
                    'to_user_id' => $id
                ]);
            })
            ->get();
    }

    public function sendMessage(array $data)
    {
        return $this->model->create($data);
    }
}
