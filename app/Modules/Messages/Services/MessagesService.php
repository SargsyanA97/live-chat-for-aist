<?php


namespace App\Modules\Messages\Services;


use App\Events\ChatEvent;
use App\Jobs\SendEmailJob;
use App\Modules\Messages\Models\Message;
use App\Modules\Messages\Repositories\MessagesRepository;
use Illuminate\Database\Eloquent\Collection;

class MessagesService
{
    /**
     * @var MessagesRepository
     */
    private MessagesRepository $repository;

    /**
     * UserService constructor.
     * @param MessagesRepository $repository
     */
    public function __construct(MessagesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $id
     * @return array|Collection
     */
    public function getMessages(string $id): array|Collection
    {
        return $this->repository->getMessages($id);
    }

    /**
     * @param array $data
     */
    public function sendMessage(array $data)
    {
        if (!empty($data['message'])) {
            $data['type'] = Message::TYPE_TEXT;
            unset($data['file']);
        } else{
            $data['type'] = Message::TYPE_FILE;
            $file_name = time() . '.' .request()->file('file')->getClientOriginalExtension();
            request()->file('file')->move(public_path('uploads'), $file_name);
            unset($data['message']);
            $data['file_name'] = $file_name;
        }

        $data['from_user_id'] = request()->user()->id;

        $message = $this->repository->sendMessage($data);
        $message = $this->repository->getOneMessage($message->id);

        broadcast(new ChatEvent($message))->toOthers();
        dispatch(new SendEmailJob($message->toUser->email));
    }
}
