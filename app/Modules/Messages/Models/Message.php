<?php

namespace App\Modules\Messages\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method create(array $data)
 */
class Message extends Model
{
    use HasFactory;

    protected $fillable = ['to_user_id', 'from_user_id', 'message', 'file_name', 'type'];

    const TYPE_TEXT = 0;
    const TYPE_FILE = 1;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    /**
     * @return BelongsTo
     */
    public function toUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }
}

