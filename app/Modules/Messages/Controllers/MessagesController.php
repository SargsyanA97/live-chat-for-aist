<?php

namespace App\Modules\Messages\Controllers;

use App\Http\Controllers\ApiBaseController;
use App\Modules\Messages\Requests\SendMessageRequest;
use App\Modules\Messages\Services\MessagesService;
use Illuminate\Http\JsonResponse;

class MessagesController extends ApiBaseController
{
    /**
     * @var MessagesService
     */
    private MessagesService $service;

    /**
     * UserController constructor.
     * @param MessagesService $service
     */
    public function __construct(MessagesService $service)
    {
        $this->service = $service;
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function getMessages(string $id): JsonResponse
    {
        $messages = $this->service->getMessages($id);

        return $this->responseWithData(compact('messages'));
    }

    public function sendMessage(SendMessageRequest $request) {
        $this->service->sendMessage([
            'file' => $request->file('file'),
            'message' => $request->get('message'),
            'to_user_id' => $request->get('to_user_id'),
        ]);
    }
}
