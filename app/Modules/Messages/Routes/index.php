<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Messages\Controllers\MessagesController;

Route::group(['middleware' => 'auth:api', 'prefix' => 'messages'], function () {
    Route::post('/', [MessagesController::class, 'sendMessage']);
    Route::get('/{id}', [MessagesController::class, 'getMessages']);
});
