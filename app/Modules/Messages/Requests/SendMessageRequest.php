<?php

namespace App\Modules\Messages\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
//            'message' => 'required_without:file|max:255|min:1',
//            'file' => 'required_without:message|file|max:2048',
//            'to_user_id' => 'required|exists:users,id'
        ];
    }
}
