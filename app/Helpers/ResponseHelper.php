<?php


namespace App\Helpers;


class ResponseHelper
{
    const RESPONSE_CODE_SUCCESS = 200;
    const RESPONSE_CODE_UNAUTHORIZED = 401;
    const RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500;
}
