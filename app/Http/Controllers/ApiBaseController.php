<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use Illuminate\Http\JsonResponse;

class ApiBaseController extends Controller
{
    private array $headers;

    /**
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    public function responseWithMessage(string $message, int $code = ResponseHelper::RESPONSE_CODE_SUCCESS): JsonResponse
    {
        if (!empty($this->headers)) {
            return response()->json([
                'message' => $message
            ], $code)->withHeaders($this->headers);
        }

        return response()->json([
            'message' => $message
        ], $code);
    }

    /**
     * @param array $errors
     * @param int $code
     * @return JsonResponse
     */
    public function responseWithError(array $errors, int $code = ResponseHelper::RESPONSE_CODE_INTERNAL_SERVER_ERROR): JsonResponse
    {
        if (!empty($this->headers)) {
            return response()->json([
                'errors' => $errors
            ], $code)->withHeaders($this->headers);
        }

        return response()->json([
            'errors' => $errors
        ], $code);
    }

    /**
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public function responseWithData(array $data, int $code = ResponseHelper::RESPONSE_CODE_SUCCESS): JsonResponse
    {
        if (!empty($this->headers)) {
            return response()->json($data, $code)->withHeaders($this->headers);
        }

        return response()->json($data, $code);
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setResponseHeaders(array $headers): static
    {
        $headers['Access-Control-Expose-Headers'] = implode(', ', array_keys($headers));
        $this->headers = $headers;

        return $this;
    }
}
